#!/usr/bin/env bash
sudo apt-get install -y zsh
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
chsh -s /bin/zsh
